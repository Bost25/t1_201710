package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {



	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}

	public double getMin(IntegersBag bag){
		double min = Integer.MAX_VALUE;
		double valor;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				valor = iter.next();
				if( min > valor){
					min = valor;
				}
			}

		}
		return min;		
	}


	public double getHalf(IntegersBag bag){
		return (getMax(bag) + getMin(bag)) / 2; 
	}

	
	// Devuelve el numero mas cercano a la mitad entre el mas grande y el mas peque�o de la bolsa
	//En caso de encontrar dos numeros con igual distancia a la mitad, devuelve el numero menor entre ellos dos.
	
	public double getClosestNumberToHalf(IntegersBag bag){
		double h = getHalf(bag);
		double res, v, num = 0;
		double va = Integer.MAX_VALUE;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				v = iter.next();
				if((v-h) > 0){
					res = v-h;
				}
				else{
					res = h-v;
				}

				if(res < va ){
					va = res;
					num = v;
				}
			}

		}
		return num;
	}

	
	
	
	//	public double getDecimals(IntegersBag bag){
	//		String pal;
	//		char[] arr;
	//		double cont = 0;
	//		if(bag != null){
	//			Iterator<Integer> iter = bag.getIterator();
	//			while(iter.hasNext()){
	//				pal = iter.next() + "";
	//				arr = new char[pal.length()];
	//				arr = pal.toCharArray();
	//				for(int i = 0; i < arr.length; i++){
	//					if(arr[i] == '.'){
	//						cont ++;
	//						break;
	//					}
	//				}
	//			}
	//		}
	//		return cont;
	//	}



	//	public double getNumbersStartingWith(IntegersBag bag, int n){
	//		double cont = 0;
	//		String pal;
	//		if(bag != null){
	//			Iterator<Integer> iter = bag.getIterator();
	//			while(iter.hasNext()){
	//				double num = iter.next();
	//				pal = num + "";
	//				if(pal.startsWith("n")){
	//					cont++;
	//				}
	//			}
	//		}
	//		return cont;
	//	}

}
